<?php

namespace Torside\PhoneNumbers\PhoneNumber;

use Illuminate\Contracts\Support\Arrayable;

final class PhoneNumber implements Arrayable
{
    /** @var int $countryCode */
    private $countryCode;

    /** @var string $region */
    private $region;

    /** @var string $standard */
    private $standard;

    /** @var string $international */
    private $international;

    /** @var string $national */
    private $national;

    /** @var int $type */
    private $type;

    /** @var string $location */
    private $location;

    /** @var string $carrier */
    private $carrier;

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'countryCode' => $this->getCountryCode(),
            'region' => $this->getRegion(),
            'standard' => $this->getStandard(),
            'international' => $this->getInternational(),
            'national' => $this->getNational(),
            'type' => $this->getType(),
            'location' => $this->getLocation(),
            'carrier' => $this->getCarrier()
        ];
    }

    /**
     * @param int $countryCode
     * @return PhoneNumber
     */
    public function setCountryCode(int $countryCode): PhoneNumber
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountryCode(): int
    {
        return $this->countryCode;
    }

    /**
     * @param string $region
     * @return PhoneNumber
     */
    public function setRegion(string $region): PhoneNumber
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $standard
     * @return PhoneNumber
     */
    public function setStandard(string $standard): PhoneNumber
    {
        $this->standard = $standard;
        return $this;
    }

    /**
     * @return string
     */
    public function getStandard(): string
    {
        return $this->standard;
    }

    /**
     * @param string $international
     * @return PhoneNumber
     */
    public function setInternational(string $international): PhoneNumber
    {
        $this->international = $international;
        return $this;
    }

    /**
     * @return string
     */
    public function getInternational(): string
    {
        return $this->international;
    }

    /**
     * @param string $national
     * @return PhoneNumber
     */
    public function setNational(string $national): PhoneNumber
    {
        $this->national = $national;
        return $this;
    }

    /**
     * @return string
     */
    public function getNational(): string
    {
        return $this->national;
    }

    /**
     * @param int $type
     * @return PhoneNumber
     */
    public function setType(int $type): PhoneNumber
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param string $location
     * @return PhoneNumber
     */
    public function setLocation(string $location): PhoneNumber
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $carrier
     * @return PhoneNumber
     */
    public function setCarrier(string $carrier): PhoneNumber
    {
        $this->carrier = $carrier;
        return $this;
    }

    /**
     * @return string
     */
    public function getCarrier(): string
    {
        return $this->carrier;
    }
}