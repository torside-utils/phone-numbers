<?php

namespace Torside\PhoneNumbers\PhoneNumber;

use Illuminate\Support\Collection;

final class PhoneNumberCollection extends Collection
{
    /**
     * Create a new collection.
     *
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }

    /**
     * Set the item at a given offset.
     *
     * @param  mixed $key
     * @param  mixed $value
     *
     * @return void
     */
    public function offsetSet($key, $value)
    {
        if ($value instanceof PhoneNumber) {
            if (is_null($key)) {
                $this->items[] = $value;
            } else {
                $this->items[$key] = $value;
            }
        }
    }
}