<?php

namespace Torside\PhoneNumbers;

use libphonenumber\geocoding\PhoneNumberOfflineGeocoder;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber as LibPhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberToCarrierMapper;
use libphonenumber\PhoneNumberType as LibPhoneNumberType;
use libphonenumber\PhoneNumberUtil as LibPhoneNumberUtil;
use Torside\PhoneNumbers\PhoneNumber\PhoneNumber;
use Torside\PhoneNumbers\PhoneNumber\PhoneNumberCollection;
use Torside\PhoneNumbers\PhoneNumber\PhoneNumberException;
use Torside\PhoneNumbers\PhoneNumberType\PhoneNumberType;
use Torside\PhoneNumbers\PhoneNumberType\PhoneNumberTypeCollection;

final class PhoneNumberService
{
    /** @var LibPhoneNumberUtil $libPhoneNumberUtil */
    private $libPhoneNumberUtil;

    /** @var PhoneNumberOfflineGeocoder $geocoder */
    private $geocoder;

    /** @var PhoneNumberToCarrierMapper $carrierMapper */
    private $carrierMapper;

    /** @var string[] $regions */
    private $regions = [];

    /**
     * PhoneNumberService constructor.
     *
     * @param LibPhoneNumberUtil $libPhoneNumberUtil
     * @param PhoneNumberOfflineGeocoder $geocoder
     * @param PhoneNumberToCarrierMapper $carrierMapper
     */
    public function __construct(LibPhoneNumberUtil $libPhoneNumberUtil, PhoneNumberOfflineGeocoder $geocoder, PhoneNumberToCarrierMapper $carrierMapper)
    {
        $this->libPhoneNumberUtil = $libPhoneNumberUtil;
        $this->geocoder = $geocoder;
        $this->carrierMapper = $carrierMapper;
    }

    /**
     * @param string $region
     * @param int $priority
     *
     * @return PhoneNumberService
     */
    public function addRegion(string $region, int $priority = 0): PhoneNumberService
    {
        foreach ($this->regions as $r) {
            if ($r['region'] === $region) {
                return $this;
            }
        }

        $this->regions[] = [
            'region' => $region,
            'priority' => $priority
        ];

        return $this;
    }

    /**
     * @param string $no
     * @param string $region
     *
     * @return PhoneNumber
     *
     * @throws PhoneNumberException
     */
    public function createPhoneNumber(string $no, string $region = ''): PhoneNumber
    {
        $region = strtoupper($region);

        try {
            /** @var LibPhoneNumber $libPhoneNumber */
            $libPhoneNumber = $this->libPhoneNumberUtil->parse($no, $region ?: null);

            if ($this->libPhoneNumberUtil->getNumberType($libPhoneNumber) === LibPhoneNumberType::UNKNOWN) {
                throw new PhoneNumberException('Invalid Phone Number Type');
            }

            /** @var PhoneNumber $phoneNumber */
            $phoneNumber = new PhoneNumber();
            $phoneNumber->setCountryCode($libPhoneNumber->getCountryCode())
                ->setRegion($this->libPhoneNumberUtil->getRegionCodeForNumber($libPhoneNumber))
                ->setStandard($this->libPhoneNumberUtil->format($libPhoneNumber, PhoneNumberFormat::E164))
                ->setInternational($this->libPhoneNumberUtil->format($libPhoneNumber, PhoneNumberFormat::INTERNATIONAL))
                ->setNational($this->libPhoneNumberUtil->format($libPhoneNumber, PhoneNumberFormat::NATIONAL))
                ->setType($this->libPhoneNumberUtil->getNumberType($libPhoneNumber))
                ->setLocation($this->geocoder->getDescriptionForNumber($libPhoneNumber, $region))
                ->setCarrier($this->carrierMapper->getNameForNumber($libPhoneNumber, $region));

            return $phoneNumber;
        } catch (NumberParseException $e) {
            throw new PhoneNumberException('Invalid Phone Number');
        }
    }

    /**
     * @param string $no
     *
     * @return PhoneNumberCollection
     */
    public function createPhoneNumberForRegions(string $no): PhoneNumberCollection
    {
        usort($this->regions, function($a, $b) {
            return $b['priority'] <=> $a['priority'];
        });

        /** @var array $phoneNumbers */
        $phoneNumbers = [];

        foreach ($this->regions as $region) {
            try {
                /** @var PhoneNumber $poneNumber */
                $phoneNumber = $this->createPhoneNumber($no, $region['region']);
            } catch (PhoneNumberException $e) {
                continue;
            }

            if (!empty($phoneNumber->getCountryCode()) && $this->isPhoneNumberValid($phoneNumber)) {
                $phoneNumbers[$phoneNumber->getCountryCode()] = $phoneNumber;
            }
        }

        return new PhoneNumberCollection(array_values($phoneNumbers));
    }

    /**
     * @param PhoneNumber $phoneNumber
     *
     * @return bool
     */
    public function isPhoneNumberValid(PhoneNumber $phoneNumber): bool
    {
        try {
            /** @var LibPhoneNumber $libPhoneNumber */
            $libPhoneNumber = $this->libPhoneNumberUtil->parse($phoneNumber->getStandard(), $phoneNumber->getRegion());

            if ($this->libPhoneNumberUtil->getNumberType($libPhoneNumber) === LibPhoneNumberType::UNKNOWN) {
                return false;
            }

            return $phoneNumber->getCountryCode() === $libPhoneNumber->getCountryCode()
                && $phoneNumber->getRegion() === $this->libPhoneNumberUtil->getRegionCodeForNumber($libPhoneNumber)
                && $phoneNumber->getStandard() === $this->libPhoneNumberUtil->format($libPhoneNumber, PhoneNumberFormat::E164)
                && $phoneNumber->getInternational() === $this->libPhoneNumberUtil->format($libPhoneNumber, PhoneNumberFormat::INTERNATIONAL)
                && $phoneNumber->getNational() === $this->libPhoneNumberUtil->format($libPhoneNumber, PhoneNumberFormat::NATIONAL)
                && $phoneNumber->getType() === $this->libPhoneNumberUtil->getNumberType($libPhoneNumber);

        } catch (NumberParseException $e) {
            return false;
        }
    }

    /**
     * @return PhoneNumberTypeCollection
     */
    public function getTypes(): PhoneNumberTypeCollection
    {
        /** @var array $types */
        $types = [];

        foreach (LibPhoneNumberType::values() as $id => $type) {
            $types[] = new PhoneNumberType($id, $type);
        }

        return new PhoneNumberTypeCollection($types);
    }
}