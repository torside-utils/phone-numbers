<?php

namespace Torside\PhoneNumbers\PhoneNumberType;

use Illuminate\Contracts\Support\Arrayable;

final class PhoneNumberType implements Arrayable
{
    /** @var int $id */
    private $id;

    /** @var string $type */
    private $type;

    /**
     * PhoneNumberType constructor.
     *
     * @param int $id
     * @param string $type
     */
    public function __construct(int $id, string $type)
    {
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}