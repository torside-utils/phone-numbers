<?php

namespace Torside\PhoneNumbers\PhoneNumberType;

use Illuminate\Support\Collection;

final class PhoneNumberTypeCollection extends Collection
{
    /**
     * Create a new collection.
     *
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }

    /**
     * Set the item at a given offset.
     *
     * @param  mixed $key
     * @param  mixed $value
     *
     * @return void
     */
    public function offsetSet($key, $value)
    {
        if ($value instanceof PhoneNumberType) {
            $this->items[] = $value;
        }
    }
}