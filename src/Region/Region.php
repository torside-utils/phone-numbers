<?php

namespace Torside\PhoneNumbers\Region;

final class Region
{
    /** @var string $locale */
    private $locale;

    /** @var array $countries */
    private $countries;

    /**
     * Region constructor.
     *
     * @param string $locale
     * @param array $countries
     */
    public function __construct(string $locale, array $countries)
    {
        $this->locale = strtoupper($locale);
        $this->countries = $countries;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countries;
    }
}