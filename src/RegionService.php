<?php

namespace Torside\PhoneNumbers;

use Torside\PhoneNumbers\Region\Region;

final class RegionService
{
    /** @var string $dataPath */
    private $dataPath;

    /**
     * RegionService constructor.
     *
     * @param string $dataPath
     */
    public function __construct(string $dataPath = './../../../giggsey/locale/data')
    {
        $this->dataPath = $dataPath;
    }

    /**
     * @param string $locale
     *
     * @return null|Region
     */
    public function createRegion(string $locale)
    {
        $locale = strtolower($locale);

        /** @var string $file */
        $file = "{$this->dataPath}/{$locale}.php";

        if (file_exists($file)) {
            /** @var array $countries */
            $countries = require $file;
        }

        if (empty($countries)) {
            return null;
        }

        return new Region($locale, $countries);
    }

    /**
     * @param Region $region
     * @param string $locale
     *
     * @return string
     */
    public function getCountryName(Region $region, string $locale)
    {
        /** @var array $countries */
        $countries = $region->getCountries();

        $locale = strtoupper($locale);

        return empty($countries[$locale]) ? '' : $countries[$locale];
    }

    /**
     * @return array
     */
    public function getListOfLocales(): array
    {
        /** @var array $localeFiles */
        $localeFiles = array_filter(scandir($this->dataPath), function (string $file) {
            return !($file === '.' || $file === '..' || substr($file, 0, 1) === '_');
        });

        return array_values(
            array_map(function (string $file) {
                return str_replace('.php', '', $file);
            }, $localeFiles)
        );
    }
}